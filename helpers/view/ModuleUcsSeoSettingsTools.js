var ModuleUcsSeoSettingsTools = (function () {

    return {
        processDomainWhitelist: processDomainWhitelist,
    };

    function processDomainWhitelist(package, domainWhitelist) {
        if (package._processDomainWhitelist) {
            return package;
        }
        
        // mark processing
        package._processDomainWhitelist = true;
        
        var arUrls = [];
        for (var i=0, iMax=domainWhitelist.length; i<iMax; i++) {
            arUrls.push(domainWhitelist[i].url);
        }
        
        if (package.spine[0] && package.spine[0].idref) {
            var blocks = package.manifest[package.spine[0].idref].content.blocks;
        
            for (var i=0, iMax=blocks.length; i<iMax; i++) {
                var block = blocks[i];
                
                if (block.type == 'paragraph' && block.data && block.data.markups) {
                    var keys = Object.keys(block.data.markups);
                    for (var j=0, jMax=keys.length; j<jMax; j++) {
                        var markup = block.data.markups[keys[j]];
                        
                        if (markup.role == 'link' && markup.url) {
                            //url
                            var found = markup.url;
                            for (var k=0, kMax=arUrls.length; k<kMax; k++) {
                                if (markup.url.indexOf(arUrls[k]) > -1) {
                                    // OK
                                    found = false;
                                    break;
                                } else {
                                    // no follow
                                    found = markup.url;
                                }
                            }
                            if (found) {
                                block.data.text = block.data.text.replace('<a href="' + found,  '<a rel="nofollow" href="' + found);
                                block.render.value = block.render.value.replace('<a href="' + found,  '<a rel="nofollow" href="' + found);
                            }
                        }
                    }
                }
            }
        }
        
        return package;
    }
    
}());

module.exports = ModuleUcsSeoSettingsTools;
